package pl.streaming

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.CommittableOffsetBatch
import akka.kafka._
import akka.kafka.scaladsl.Consumer._
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.typesafe.config._
import com.typesafe.scalalogging.LazyLogging
import org.apache.kafka.clients.producer.ProducerRecord
import pl.streaming.dsl.DataTarget.{BlackHole, Topic}
import pl.streaming.dsl.{DataSource, DataTarget}

import scala.concurrent.Future

class StreamProcessor[K1, V1, K2, V2](
  dataSource: DataSource,
  parallelism: Int,
  f: (K1, V1) => Future[(K2, V2)],
  target: DataTarget
)(implicit system: ActorSystem) extends LazyLogging {

  private implicit val materializer = ActorMaterializer()
  private implicit val executionContext = system.dispatcher

  def run() = {
    val head =
      source
        .groupBy(parallelism, x => x.record.partition() % parallelism)

    target match {
      case BlackHole =>
        head
          .mapAsync(1) {
            x =>
              logger.debug(s"Received message with offset: ${x.committableOffset} for ${x.record.value()}")
              f(x.record.key(), x.record.value()).map(_ => x.committableOffset)
          }
          .async
          .batch(max = 10, first => CommittableOffsetBatch.empty.updated(first)) {
            (batch, elem) => batch.updated(elem)
          }
          .mapAsync(1)(commitRecord)
          .mergeSubstreams
          .runWith(Sink.ignore)

      case Topic(name, config) =>
        head
          .mapAsync(1) {
            x =>
              logger.debug(s"Received message with offset: ${x.committableOffset} for ${x.record.value()}")
              f(x.record.key(), x.record.value()).map {
                case (rKey, rValue) =>
                  ProducerMessage.Message(new ProducerRecord(name, rKey, rValue), x.committableOffset)
              }

          }
          .via(target(config))
          .async
          .map(_.message.passThrough)
          .batch(max = 10, first => CommittableOffsetBatch.empty.updated(first)) {
            (batch, elem) => batch.updated(elem)
          }
          .mapAsync(1)(commitRecord)
          .mergeSubstreams
          .runWith(Sink.ignore)
    }
  }

  lazy val source = {
    val config = dataSource.config.withValue("kafka-clients.enable.auto.commit", ConfigValueFactory.fromAnyRef(false))
    committableSource(ConsumerSettings[K1, V1](config), Subscriptions.topics(dataSource.topic))
  }

  def target(config: Config) = {
    val producerSettings = ProducerSettings[K2, V2](config)
    val sink = Producer.flow[K2, V2, ConsumerMessage.CommittableOffset](producerSettings)
    sink
  }

  def commitRecord(x: ConsumerMessage.Committable): Future[Done] = {
    logger.debug(s"Committing: $x")
    x.commitScaladsl()
  }
}