package pl.streaming.dsl

import com.typesafe.config.Config

case class DataSource(
  topic: String,
  config: Config
)