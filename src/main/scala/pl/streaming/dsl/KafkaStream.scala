package pl.streaming.dsl

import com.typesafe.config.Config

import scala.concurrent.Future

case class KafkaStream(
  topic: String,
  sourceConfig: Config
) {

  private lazy val dataSource = DataSource(topic, sourceConfig)

  def mapAsync[K1, V1, K2, V2](parallelism: Int)(f: (K1, V1) => Future[(K2, V2)]) = KafkaStreamMapper(dataSource, parallelism, f)

}
