package pl.streaming.dsl

import akka.actor.ActorSystem
import pl.streaming.StreamProcessor

import scala.concurrent.Future


private[dsl] case class CompleteStream[K1, V1, K2, V2, T <: DataTarget](
  dataSource: DataSource,
  parallelism: Int,
  f: (K1, V1) => Future[(K2, V2)],
  dataTarget: T
) {
  def run()(implicit system: ActorSystem) = {
    new StreamProcessor(dataSource, parallelism, f, dataTarget).run()
  }
}