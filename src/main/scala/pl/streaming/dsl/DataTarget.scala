package pl.streaming.dsl

import com.typesafe.config.Config

sealed trait DataTarget

object DataTarget {

  case class Topic(
    topic: String,
    config: Config
  ) extends DataTarget

  /**
    * This means that results of processing are ignored. Side effects are what matters when processing like this.
    */
  case object BlackHole extends DataTarget

}
