package pl.streaming.dsl

import akka.actor.ActorSystem
import com.typesafe.config.Config

import scala.concurrent.Future


private[dsl] case class KafkaStreamMapper[K1, V1, K2, V2](
  dataSource: DataSource,
  parallelism: Int,
  f: (K1, V1) => Future[(K2, V2)]
) {
  def to(topic: String, producerConfig: Config) = CompleteStream(dataSource, parallelism, f, DataTarget.Topic(topic, producerConfig))

  /**
    * Processing results are not sent anywhere. This depends only on mapping side effects (storage, sending, etc.).
    */
  def run()(implicit system: ActorSystem) = CompleteStream(dataSource, parallelism, f, DataTarget.BlackHole).run()
}
