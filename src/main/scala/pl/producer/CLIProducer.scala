package pl.producer

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import org.apache.kafka.clients.producer.ProducerRecord

import scala.collection.immutable
import scala.concurrent.Future

object CLIProducer extends App {


  implicit val system = ActorSystem("Producer")
  implicit val materializer = ActorMaterializer()

  val config = system.settings.config.getConfig("kafka.producer")

//  private val producerSettings = ProducerSettings[Nothing, String](config)
//  private val sink: Sink[ProducerRecord[Nothing, String], Future[Done]] = Producer.plainSink[Nothing, String](producerSettings)
//  private val iterable: immutable.Iterable[ProducerRecord[Nothing, String]] =
//    Stream.from(0).map { x =>
//        Thread.sleep(400)
//        new ProducerRecord("numbers", x.toString)
//      }
//
//  Source(iterable).runWith(sink)

  private val producerSettings = ProducerSettings[String, String](config)
  private val sink: Sink[ProducerRecord[String, String], Future[Done]] = Producer.plainSink[String, String](producerSettings)
  private val iterable: immutable.Iterable[ProducerRecord[String, String]] =
    Stream.from(0).map { x =>
        Thread.sleep(1000)
        new ProducerRecord("numbers", (x % 6).toString, x.toString)
      }

  Source(iterable).runWith(sink)


}
