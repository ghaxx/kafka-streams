package pl.consumer

import akka.actor.ActorSystem
import com.typesafe.config.Config
import pl.streaming.dsl.KafkaStream

import scala.concurrent.Future

object StreamsApp extends App {

  implicit val system = ActorSystem("Sys")
  implicit val executionContext = system.dispatcher

  val a = Array(0, 1, 2, 3).iterator
  val a1 = a.drop(2)
  val a2 = a.drop(2)
  val a3 = a.drop(2)

  def f(key: String, value: String): Future[(String, String)] = {
    println("processing = " + (key -> value))
    Thread.sleep(1500)
    Future successful (key, value)
  }

  def g(key: String, value: String): (String, String) = {
    println("processing = " + (key -> value))
    Thread.sleep(1500)
    (key, value + "_asdf")
  }

  private val parallelism: Int = system.settings.config.getInt("numbers.parallelism")
  private val consumerConfig: Config = system.settings.config.getConfig("numbers.consumer")
  private val producerConfig: Config = system.settings.config.getConfig("numbers.producer")

  KafkaStream("numbers", consumerConfig)
    .mapAsync(parallelism)(f _)
    .to("squares", producerConfig)
    .run()

}
