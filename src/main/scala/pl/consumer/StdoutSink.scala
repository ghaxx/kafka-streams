package pl.consumer

import akka.stream.{Attributes, Inlet, SinkShape}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler}
import org.apache.kafka.clients.consumer.ConsumerRecord

class StdoutSink extends GraphStage[SinkShape[ConsumerRecord[String, String]]] {
  val in: Inlet[ConsumerRecord[String, String]] = Inlet("StdoutSink")
  override val shape: SinkShape[ConsumerRecord[String, String]] = SinkShape(in)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {

      // This requests one element at the Sink startup.
      override def preStart(): Unit = pull(in)

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          println(grab(in))
//          Thread.sleep(1000)
          pull(in)
        }
      })
    }
}