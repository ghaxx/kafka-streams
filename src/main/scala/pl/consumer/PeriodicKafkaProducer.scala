package pl.consumer

import akka.stream.stage._
import akka.stream.{Attributes, Outlet, SourceShape}

class PeriodicKafkaProducer extends GraphStage[SourceShape[Int]] {
  val out: Outlet[Int] = Outlet("NumbersSource")
  override val shape: SourceShape[Int] = SourceShape(out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new TimerGraphStageLogic(shape) {

      import scala.concurrent.duration._

      // All state MUST be inside the GraphStageLogic,
      // never inside the enclosing GraphStage.
      // This state is safe to access and modify from all the
      // callbacks that are provided by GraphStageLogic and the
      // registered handlers.
      private var counter = 1

      def pushNext() = {
        push(out, counter)
        counter += 1
      }

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
//          scheduleOnce("key", 400 millis)
        }
      })

      @scala.throws[Exception](classOf[Exception])
      override def preStart(): Unit = {
        schedulePeriodically("key", 400 millis)
      }

      @scala.throws[Exception](classOf[Exception]) override protected
      def onTimer(timerKey: Any): Unit = {
        println("timerKey = " + timerKey)
        pushNext()
      }
    }
}